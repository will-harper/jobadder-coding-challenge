﻿using jobadder.core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jobadder.core.Entities
{
    public class Job : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Company { get; set; }
        public string Skills { get; set; }
        public IEnumerable<string> GetSkillsAsStrings()
        {
            return Skills.Split(',').Select(i => i.Trim());
        }
    }
}
