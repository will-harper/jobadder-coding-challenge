﻿using jobadder.core.Entities;
using jobadder.core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jobadder.core.Services
{
    public class JobsService : IJobsService
    {
        private readonly IRepository<Job> _jobsRepo;
        private readonly IRepository<Candidate> _candidatessRepo;

        public JobsService(IRepository<Job> jobsRepo, IRepository<Candidate> candidatessRepo)
        {
            _jobsRepo = jobsRepo;
            _candidatessRepo = candidatessRepo;
        }

        public async Task<IEnumerable<Candidate>> GetHighestRatedCandidates(int jobId)
        {
            var job = await _jobsRepo.GetById(jobId);
           
            var jobSkills = job.GetSkillsAsStrings().Select(i => i.ToLower());

            return (await _candidatessRepo.ListAll())
                                .Where(i => i.GetSkillsAsStrings().Select(k => k.ToLower()).Count(j => jobSkills.Contains(j)) > 1);
        }

        public async Task<Job> GetJob(int jobId)
        {
            return await _jobsRepo.GetById(jobId);
        }

        public async Task<IEnumerable<Job>> GetJobs()
        {
            return await _jobsRepo.ListAll();
        }
    }
}
