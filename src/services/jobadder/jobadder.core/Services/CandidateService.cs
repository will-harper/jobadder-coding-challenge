﻿using jobadder.core.Entities;
using jobadder.core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace jobadder.core.Services
{
    public class CandidateService : ICandidateService
    {
        private readonly IRepository<Candidate> _candidateRepo;

        public CandidateService(IRepository<Candidate> candidateRepo)
        {
            _candidateRepo = candidateRepo;
        }

        public async Task<Candidate> GetCandidate(int candidateId)
        {
            return await _candidateRepo.GetById(candidateId);
        }

        public async Task<IEnumerable<Candidate>> GetCandidates()
        {
            return await _candidateRepo.ListAll();
        }
    }
}
