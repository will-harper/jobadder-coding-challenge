﻿using jobadder.core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace jobadder.core.Interfaces
{
    public interface ICandidateService
    {
        Task<Candidate> GetCandidate(int candidateId);
        Task<IEnumerable<Candidate>> GetCandidates();

    }
}
