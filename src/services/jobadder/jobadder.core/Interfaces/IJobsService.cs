﻿using jobadder.core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace jobadder.core.Interfaces
{
    public interface IJobsService
    {
        Task<IEnumerable<Candidate>> GetHighestRatedCandidates(int jobId);

        Task<Job> GetJob(int jobId);
        Task<IEnumerable<Job>> GetJobs();
    }
}
