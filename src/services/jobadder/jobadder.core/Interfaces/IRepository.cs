﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace jobadder.core.Interfaces
{
    public interface IRepository<T> where T : IEntity
    {
        Task<T> Add(T entity);
        Task<IEnumerable<T>> ListAll();
        Task<T> GetById(int id);
        Task Delete(T entity);
        Task<T> Update(T entity);
    }
}
