﻿using jobadder.core.Entities;
using jobadder.core.Interfaces;
using jobadder.infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace jobadder.infrastructure.Repositories
{

    public class CandidatesRepository : IRepository<Candidate>
    {
        private JobsContext _context { get; set; }

        public CandidatesRepository(JobsContext context)
        {
            _context = context;
        }

        public async Task<Candidate> Add(Candidate entity)
        {
            var addedCandidate = await _context.Candidates.AddAsync(entity);

            var saveTask = await _context.SaveChangesAsync();

            return addedCandidate.Entity;
        }

        public async Task Delete(Candidate entity)
        {
            _context.Candidates.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<Candidate> GetById(int id)
        {
            return await _context.Candidates.FindAsync(id);            
        }

        public async Task<IEnumerable<Candidate>> ListAll()
        {
            return await _context.Candidates.ToListAsync();
        }

        public async Task<Candidate> Update(Candidate entity)
        {
            var updatedEntity = _context.Candidates.Update(entity);
            await _context.SaveChangesAsync();

            return updatedEntity.Entity;
        }
    }
}
