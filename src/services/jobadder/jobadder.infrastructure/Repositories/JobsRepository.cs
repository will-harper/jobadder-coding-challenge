﻿using jobadder.core.Entities;
using jobadder.core.Interfaces;
using jobadder.infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace jobadder.infrastructure.Repositories
{
    public class JobsRepository : IRepository<Job>
    {
        private JobsContext _context { get; set; }

        public JobsRepository(JobsContext context)
        {
            _context = context;
        }
        public async Task<Job> Add(Job entity)
        {
            var addedJob = await _context.Jobs.AddAsync(entity);

            var saveTask = await _context.SaveChangesAsync();
            
            return addedJob.Entity;
        }

        public async Task Delete(Job entity)
        {
            _context.Jobs.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<Job> GetById(int id)
        {
            return await _context.Jobs.FirstOrDefaultAsync(i => i.Id == id);            
        }

        public async Task<IEnumerable<Job>> ListAll()
        {
            return await _context.Jobs.ToListAsync();
        }
        public Task<Job> Update(Job entity)
        {
            throw new NotImplementedException();
        }
    }
}

