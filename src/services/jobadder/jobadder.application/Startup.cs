﻿using jobadder.core.Entities;
using jobadder.core.Interfaces;
using jobadder.core.Services;
using jobadder.infrastructure.Contexts;
using jobadder.infrastructure.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace jobadder.application
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<JobsContext>(options => options.UseSqlServer(Configuration.GetConnectionString("JobsContext")));

            services.AddScoped<IRepository<Job>, JobsRepository>();
            services.AddScoped<IRepository<Candidate>, CandidatesRepository>();

            services.AddScoped<IJobsService, JobsService>();
            services.AddScoped<ICandidateService, CandidateService>();

            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo { Title = "Job Adder Tech Test API", Version = "v1" });
            });

            services.AddCors(options => {
                options.AddPolicy("localhost", 
                builder => {
                    builder.WithOrigins("http://localhost");
                });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(i =>
                {
                    i.SwaggerEndpoint("/swagger/v1/swagger.json", "Job Adder Tech Test API");
                });

            }
            app.UseCors("localhost");

            UpdateDatabase(app);

            app.UseHttpsRedirection();
            app.UseMvc();
        }
        
        private void UpdateDatabase(IApplicationBuilder app)
        {
            using(var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                using(var context = scope.ServiceProvider.GetService<JobsContext>())
                {
                    context.Database.Migrate();
                }
            }
        }
    }
}
