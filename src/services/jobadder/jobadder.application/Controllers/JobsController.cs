﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using jobadder.core.Entities;
using jobadder.core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace jobadder.application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : ControllerBase
    {
        private readonly IJobsService _jobService;

        public JobsController(IJobsService jobsService)
        {
            _jobService = jobsService;
        }
        
        [HttpGet]
        public async Task<IEnumerable<Job>> Get()
        {
            return await _jobService.GetJobs();
        }

        [HttpGet("{id}")]
        public async Task<Job> Get(int id)
        {
            return await _jobService.GetJob(id);
        }
        
        [HttpGet("{id}/best-candidates")]
        public async Task<IEnumerable<Candidate>> GetHighestRankingCandidates(int id)
        {
            return await _jobService.GetHighestRatedCandidates(id);
        }
    }
}
