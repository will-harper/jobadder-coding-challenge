﻿using System.Collections.Generic;
using System.Threading.Tasks;
using jobadder.core.Entities;
using jobadder.core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace jobadder.application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CandidatesController : ControllerBase
    {
        private readonly ICandidateService _candidateService;

        public CandidatesController(ICandidateService candidateService)
        {
            _candidateService = candidateService;
        }

        [HttpGet]
        public async Task<IEnumerable<Candidate>> Get()
        {
            return await _candidateService.GetCandidates();
        }

        [HttpGet("{id}")]
        public async Task<Candidate> Get(int id)
        {
            return await _candidateService.GetCandidate(id);
        }
    }
}
