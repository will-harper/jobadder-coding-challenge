export class Job {
    id: number;
    name: string;
    company: string;
    skills: string;
}