export class Candidate {
    id: number;
    name: string;
    skills: string;
}