import { Injectable } from '@angular/core';
import { Candidate } from './models/Candidate';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {
  private candidateUrl = `${environment.baseUrl}/api/candidates`;

  constructor(private http: HttpClient) { }

  getCandidates(): Observable<Candidate[]>{
    return this.http.get<Candidate[]>(this.candidateUrl);
  }

  getCandidate(id: number) : Observable<Candidate>{
    return this.http.get<Candidate>(`${this.candidateUrl}/${id}`);
  }

}
