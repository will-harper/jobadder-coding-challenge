import { Component, OnInit, Input } from '@angular/core';
import { Job } from '../models/Job';
import { ActivatedRoute } from '@angular/router';
import { JobService } from '../job.service';
import { Candidate } from '../models/Candidate';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.less']
})
export class JobComponent implements OnInit {
  job: Job;
  bestCandidates: Candidate[]
  constructor(private route: ActivatedRoute, 
    private jobService : JobService) { }

  ngOnInit() : void{
    this.getJob();
    this.getBestCandidates();
  }
  getJob() : void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.jobService.getJob(id).subscribe(s => this.job = s);    
  }
  getBestCandidates(): void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.jobService.getBestCandidatesForJob(id).subscribe(s => this.bestCandidates = s);  
  }
}
