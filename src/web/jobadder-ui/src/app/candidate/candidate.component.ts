import { Component, OnInit, Input } from '@angular/core';
import { Candidate } from '../models/Candidate';
import { CandidateService } from '../candidate.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.less']
})
export class CandidateComponent implements OnInit {
  candidate: Candidate;
  constructor(private route: ActivatedRoute,private candidateService : CandidateService) { }

  ngOnInit() {
    this.getCandidate();
  }

  getCandidate(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.candidateService.getCandidate(id).subscribe(s => this.candidate = s);    
  }

}
