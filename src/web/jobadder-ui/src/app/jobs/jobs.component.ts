import { Component, OnInit } from '@angular/core';
import { Job } from '../models/Job';
import { JobService } from '../job.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.less']
})
export class JobsComponent implements OnInit {
  jobs: Job[];

  constructor(private jobService : JobService) { }

  ngOnInit() {
    this.getJobs();
  }

  getJobs(): void {
    this.jobService.getJobs().subscribe(j => this.jobs = j);
  }
}