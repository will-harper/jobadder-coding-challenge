import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandidatesComponent } from './candidates/candidates.component'
import { CandidateComponent } from './candidate/candidate.component'
import { JobsComponent } from './jobs/jobs.component';
import { JobComponent } from './job/job.component';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'candidates', component: CandidatesComponent },
  { path: 'jobs', component: JobsComponent },
  { path: 'job/:id', component: JobComponent},
  { path: 'candidate/:id', component: CandidateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
