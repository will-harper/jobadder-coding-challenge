import { Injectable } from '@angular/core';
import { Job } from './models/Job';
import { Candidate } from './models/Candidate';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JobService {
  private jobUrl = `${environment.baseUrl}/api/jobs`;

  constructor(private http: HttpClient) { }

  getJobs(): Observable<Job[]>{
    return this.http.get<Job[]>(this.jobUrl);
  }

  getJob(id: number) : Observable<Job>{
    return this.http.get<Job>(this.jobUrl);
  }

  getBestCandidatesForJob(id: number) : Observable<Candidate[]>{
    return this.http.get<Candidate[]>(`${this.jobUrl}/${id}/best-candidates`);
  }
}