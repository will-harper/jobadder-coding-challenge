import { Component, OnInit, Input } from '@angular/core';
import { Candidate } from '../models/Candidate';
import { CandidateService } from '../candidate.service';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.less']
})
export class CandidatesComponent implements OnInit {
  candidates: Candidate[];
  
  constructor(private candidateService : CandidateService) { }

  getCandidates(): void {
    this.candidateService.getCandidates().subscribe(s => this.candidates = s);
  }

  ngOnInit() {
    this.getCandidates();
  }
}
