# jobadder-coding-challenge

This project was produced for JobAdder as part of their recruitment process.

This consists of two projects one in dotnet core 2.2 and one in Angular 8.

It utilises the following technologies in its implementation
- Angular 8
- dotnet core 2.2
- EF Core
- Docker
- SqlServer
- Swagger

##To Start

There are two options here, running it via docker or running it locally.

### With Docker

1. Install Docker
2. Open a terminal (bash or powershell)
3. Make sure the /src/services/jobadder folder doesn't have the .vs folder in it.
4. Navigate to the root directory of this repository
5. Type docker-compose up into the terminal
6. Open a browser and navigate to localhost

### Without Docker
1. Start sql server and note down the url and port it is running on
2. Navigate to /src/services/jobadder/jobadder.application
3. Open appsettings.json and change the jobs context connection string to point to where sql server is running on your machine
4. Navigate back to /src/services/jobadder and run the solution. Note down on what url and port it has started on.
5. Navigate to src/web/jobadder-ui/environments and open environment.ts. Change base url to the url and endpoint noted down in step 4.
6. Navigate to src/web/jobadder-ui and start the development server using ng server --open

# Methodology
The method used in this app to determine who the best candidates are is done by an overlap in skills between the job and the candidate. This seemed like the most straight forward way to bring the candidates who would be potentially suited to a role to the forefront and then have their other skills be judged by the user to determine if they might make them a potential candidate.

# If I Had More Time
If I had more time I would improve the look of the UI dramatically and I would improve navigate from page to page to improve the user experience.
